@extends('layouts.main')


  @section('content')
    <!-- body -->
    <section class="videos container-fluid">
        <!-- articles -->
        <!-- action -->
            <article class="drama-main mt-5">
                <div class="row mb-2">
                    <div class="col-1"></div>
                    <div class="col ms-3">
                        <!-- this prints the name of the genere -->
                        <h2>Productos</h2>
                    </div>
                </div>
                <!-- drama carousel-->
                <div class="drama row>
                    <a href="/product/1">
                        <div class="col d-flex justify-content-center pic-container shadow">
                            <img src="{{ asset('img/p1.jpg') }}" alt="producto 1">
                            <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                                <i class="bi bi-heart ms-auto me-2"></i>
                                <p class="title mx-auto mt-auto mb-0">Producto 1</p>
                            </div>
                        </div>
                    </a>

                    <a href="/product/2">
                        <div class="col d-flex justify-content-center pic-container shadow">
                            <img src="{{ asset('img/p2.jpg') }}" alt="producto 2">
                            <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                                <i class="bi bi-heart ms-auto me-2"></i>
                                <p class="title mx-auto mt-auto mb-0">Producto 2</p>
                            </div>
                        </div>
                    </a>

                    <a href="/product/3">
                        <div class="col d-flex justify-content-center pic-container shadow">
                            <img src="{{ asset('img/p3.jpg') }}" alt="producto 3">
                            <div class="caption d-flex flex-column overflow-hidden w-100 h-100">
                                <i class="bi bi-heart ms-auto me-2"></i>
                                <p class="title mx-auto mt-auto mb-0">Producto 3</p>
                            </div>
                        </div>
                    </a>


                </div>
            </article>
    </section>
@endsection