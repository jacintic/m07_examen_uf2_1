<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

   <!-- styles -->
    <link rel="stylesheet" href="{{asset('css/sapp.css')}}">
    <!-- extra CSS -->
    @yield('css')

</head>
<body>
    <!-- main container -->
    <div class="container-fluid bg-dark text-white main-cont p-0 pt-5 d-flex flex-wrap">
        <!-- header/navbar -->
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark shadow fixed-top">
                <div class="container-fluid">
                    <!-- navbar non collapsed -->
                    <section class="logo navbar-brand">
                        <img src="{{ asset('img/logo.svg') }}" alt="shop logo">
                    </section>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- navbar collapsed -->
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse row" id="navbarNav">
                            <section class="menu col-lg-2">
                                <ul class="navbar-nav">
                                    <li class="nav-item ms-lg-1">
                                        <a href="/home" class="d-flex align-items-center
                                                justify-items-center">
                                            <!-- home icon -->
                                            <i class="bi-house me-1"></i>
                                            <span>Home</span>
                                        </a>
                                    </li>
                                    <li class="nav-item ms-lg-4">
                                        <a href="watchlists" class="d-flex align-items-center
                                                justify-items-center">
                                            <!-- list icon -->
                                            <i class="bi-card-list me-1"></i>
                                            <span>Products</span>
                                        </a>
                                    </li>
                                </ul>
                            </section>
                            <section class="search col-lg-6 col-md-6">
                                <form class="d-flex" class="d-flex
                                        align-items-center
                                        justify-items-center">
                                    <input class="form-control me-2" type="search" placeholder="Search"
                                        aria-label="Search">
                                        <a href="/search" class="btn text-light">
                                            <!-- search icon -->
                                            <i class="bi-search"></i>
                                        </a>
                                </form>
                            </section>
                            <div class="col-lg">
                                <section class="user_avatar d-flex
                                        justify-content-lg-end">
                                    <a href="user_settings" class="profile-pic
                                            text-center">
                                        <!-- user icon -->
                                        <i class="bi bi-person-circle"></i>
                                        <p>Francisca</p>
                                    </a>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
@yield('content')
<!-- footer -->
<footer class="d-flex w-100 mt-auto">
    <h3 class="col-auto ms-auto me-3">&#169; 2022<img src="img/logo.svg" alt="petflix logo">. All rights reserved.</h3>
</footer>

</div>
<!-- scripts js -->
<script src="{{ asset('js/app.js') }}"></script>

<!-- extra js -->
@yield('js')
</body>

</html>