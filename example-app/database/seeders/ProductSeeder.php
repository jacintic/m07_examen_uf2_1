<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // comedy
        DB::table('movies')->insert(
            [
                [
                    'nombre' => "aspirador clase A",
                    'precio' => 50.75,
                    'descripcion' => 'Aspira poco'
                ],
                [
                    'nombre' => "aspirador clase B",
                    'precio' => 80.75,
                    'descripcion' => 'Aspira mucho'
                ],
                [
                    'nombre' => "aspirador",
                    'precio' => 75.75,
                    'descripcion' => 'Aspira medio meido'
                ]

             ]
        );
    }
}

